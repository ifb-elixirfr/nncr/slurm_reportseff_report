# slurm_reportseff_report

## Description

This python util is a wrapper around the [report_seff](https://github.com/troycomi/reportseff) tool.

It is designed to help us plan future investments. 

## Installation

### Prerequisites

 - Python
 - [Git]

```bash
git clone https://gitlab.com/ifb-elixirfr/nncr/slurm_reportseff_report.git
```

## Usage

```bash
#module load python

cd slurm_reportseff_report
./reportseff_report.py --since 2024-03-01 --until 2024-06-30

    Since 2024-03-01 - Until 2024-06-30
    Mem used 0.72 GB / CPU
    Mem req  6.91 GB / CPU
    Mem used 2.57 GB / Job
    Mem req  21.91 GB / Job
    Mem eff  10.53 / Job
    CPU req  3.7 / Job
    CPU eff  34.26 / Job
```

## Roadmap

 - [X] Some stats on the usage of our SLURM infrastructure (mem / cpu, ...)
 - [ ] Run and record on a period regularly
 - [ ] Draw some graph with class instead of a single average
 - [ ] A "Hall of shame" to spot users than could improve their practice in term of efficiency


## License
For open source projects, say how it is licensed.

