#!/usr/bin/env python3

# Usage: ./reportseff_report.py --since 2024-07-02 --until 2024-07-03 

import argparse
import datetime
import logging
from subprocess import check_output, CalledProcessError
import sys

reportseff_path = "/shared/software/miniconda/envs/reportseff-2.7.6/bin/reportseff"

def mem_in_G(mem):
    """
    Convert requested mem from 250M -> 0.25
    """
    conversion = {'T' : 1000.0, 'G' : 1.0, 'M' : 0.001}
    return (float(mem[:-1]) * conversion[mem[-1:]])        


def parse_reportseff_lines(reportseff_lines):
    reportseff_tab = {}

    # stats init
    memused_per_cpu_total = 0
    memreq_per_cpu_total = 0
    memused_total = 0
    memreq_total = 0
    memeff_total = 0
    cpusreq_total = 0
    count = 0
    cpueff_total = 0
    count_multithread = 0

    # remove first head line and parse with '|'
    for line in reportseff_lines.split('\n')[1:]:
        if line == '':
            continue

        jobid, state, elapsed, timeeff, cpueff, memeff, reqcpus, reqmem, user = line.split('|')
        
        cpueff = float(cpueff) if cpueff != "---" else "NA" 
        memeff = float(memeff) 
        reqcpus = int(reqcpus)
        reqmem = mem_in_G(reqmem)

        memused = memeff * reqmem / 100

        if user not in reportseff_tab:
            reportseff_tab[user] = []

        job = { 'jobid' : jobid, 
               'state' : state, 
               'elapsed' : elapsed, 
               'timeeff' : timeeff, 
               'cpueff' : cpueff, 
               'memeff' : memeff, 
               'reqcpus' : reqcpus, 
               'reqmem' : reqmem,
               'memused' : memused
               }
        
        reportseff_tab[user].append(job)

        # stats 
        if reqcpus > 0 and reqcpus != "NA":
            memused_per_cpu_total += memused / reqcpus
            memreq_per_cpu_total += reqmem / reqcpus
            memused_total += memused
            memreq_total += reqmem
            memeff_total += memeff
            cpusreq_total += reqcpus
            count += 1
        if reqcpus > 1 and cpueff != "NA":
            cpueff_total += cpueff
            count_multithread += 1

    print ("Mem used %s GB / CPU" % str(round(memused_per_cpu_total / count, 2)))
    print ("Mem req  %s GB / CPU" % str(round(memreq_per_cpu_total / count, 2)))
    print ("Mem used %s GB / Job" % str(round(memused_total / count, 2)))
    print ("Mem req  %s GB / Job" % str(round(memreq_total / count, 2)))
    print ("Mem eff  %s / Job" % str(round(memeff_total / count, 2)))
    print ("CPU req  %s / Job" % str(round(cpusreq_total / count, 2)))
    print ("CPU eff  %s / Job" % str(round(cpueff_total / count_multithread, 2)))

    return reportseff_tab


def run_reportseff(since, until):
    since = str(since).split(" ")[0]
    until = str(until).split(" ")[0]

    print("Since %s - Until %s" % (since, until))

    try:
        #reportseff -p -s CD  --since 2024-07-03 --format "+ReqCPUS,ReqMem,User"
        cmd = [reportseff_path, "-p", "-s", "CD", "--since", since, "--until", until, "--format", "+ReqCPUS,ReqMem,User"]
        #print (' '.join(cmd))
        output = check_output(cmd, text=True)

    except CalledProcessError as e:
        print(f"Command failed with return code {e.returncode}")
        sys.exit(1)
    return output


if __name__ == '__main__':

    # Arguments
    parser = argparse.ArgumentParser(description='This script provide module to parse reportseff report on a periode')
    parser.add_argument('--since', metavar='since', type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%d'), help='Format 2024-06-27')
    parser.add_argument('--until', metavar='until', type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%d'), help='Format 2024-06-27')
    #parser.add_argument('--get_mem_per_cpu', dest='get_mem_per_cpu', action="store_true", default=False, help="Calculate the ratio mem (GB) per cpu")
    parser.add_argument('--debug', dest='debug', action="store_true", default=False, help="Enable debug output.")

    args = parser.parse_args()

    if args.debug is True :
        logging.basicConfig(level=logging.DEBUG)

    reportseff_lines = run_reportseff(args.since, args.until)
    reportseff_tab = parse_reportseff_lines(reportseff_lines)

